# NdFeB dotfiles

My dotfiles, for anyone. Use at your own risk.

Bash only.

## Installation

```sh
# Can be cloned anywhere
git clone https://gitlab.com/NdFeB/dotfiles
bash dotfiles/install.sh
. ~/.bashrc
```

For each dotfile, the `install.sh` script will:

- Check if a file with that name already exists in `$HOME`
- If it does not, it creates a symlink with the same name
- If it does:
  - if it's a symlink pointing to a regular file, or a broken symlink, it overrides it
  - if it's a regular file, it makes a backup of it with a timestamp suffix,
    then creates a symlink
  - if it's another file type, or a symlink pointing to another file type
    (directory, block device, etc.), it will skip it and print a warning message

## Fonts

Some characters used in PS1 require appropriate fonts. I use Caskaydia Mono from NerdFonts.

To install it:

On WSL, using Windows Terminal, do the following:
- Download the font file from [NerdFonts official download page](https://www.nerdfonts.com/font-downloads)
- Open the downloaded zip file
- Double click the variant you want (for Caskaydia I use `CaskaydiaMonoNerdFontMono-Regular.ttf`)
- Click install
- After installation is complete, go in Windows Terminal settings
- Select your favorite distro, scroll down to "Additional Settings", and click "Appearance"
- In "Font face", select "Caskaydia Nerd Font"

On Debian and variants:
```bash
$ VARIANT=CaskaydiaMonoNerdFontMono-Regular.ttf
$ mkdir -p ~/.fonts
$ curl -Lo /tmp/CascadiaMono.zip 'https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/CascadiaMono.zip'
$ unzip -p /tmp/CascadiaMono.zip "$VARIANT" > ~/.fonts/"$VARIANT"
$ rm /tmp/CascadiaMono.zip
$ fc-cache -fv
```

Then configure your terminal to use it.

## About files and tools

### .gitignore

This file is not intended to be installed. It's the actual gitignore config for this repository.

### tmux

`tmux` can be pretty hard de configure sometimes. When I need to debug some conf, I use the
status bar to show relevant debug info.

To avoid having unwanted changes in `.tmux.conf`, I setup the debug stuff in a separate file,
`~/.tmux.debug.conf`. `.tmux.conf` is configured to include it, but it does not fail if it's absent.

The `.tmux.debug.example.conf` file provided here is not installed by the `install.sh` script.
