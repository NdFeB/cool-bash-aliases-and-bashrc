# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

############################## BASH CONFIG ##############################

# Don't put duplicate lines or lines starting with space in the history
HISTCONTROL=ignoreboth

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=2000
HISTFILESIZE=2000

# Append to the history file, don't overwrite it
shopt -s histappend

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# Case-insensitive globbing (used in pathname expansion)
#shopt -s nocaseglob;

# Autocorrect typos in path names when using `cd`
shopt -s cdspell;

# If set, `**/stuff` will enter `./foo/bar/stuff`
shopt -s autocd

############################## HELPERS, COLORS ##############################

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

############################## SOURCE DOT FILES ##############################

# Enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    # Global completion directory
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

# * ~/.path can be used to extend PATH
# * ~/.extra can be used for other settings you don't want to commit
# Order matters!
#   exports may be useful to completion/prompt scripts,
#   completion functions may be used as condition during aliases setup,
#   and so on
# ~/.bash_completion is automatically sourced by
# /usr/share/bash-completion/bash_completion, which is sourced earlier
for file in \
        path \
        exports \
        bash_functions \
        bash_aliases \
        bash_prompt \
        extra; do
	[ -r "${HOME}/.${file}" ] && [ -f "${HOME}/.${file}" ] && source "${HOME}/.${file}";
    #[ -r ".$file" ] && [ -f ".$file" ] && source ".$file";
done;
unset file;

############################## LAST MINUTE ##############################

command -v start_stored_ssh_agent &>/dev/null && \
    start_stored_ssh_agent

############################## OTHER ##############################

# Let this NPM config here or NPM will be sad
# NPM config
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
