set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab
set mouse=
set ttymouse=

set number
filetype plugin indent on

set showcmd
set showmatch

set ignorecase " case insensitive search
set smartcase  " case insensitive only if term is all lower case, else case sensitive
set incsearch  " Incremental search (jump to matching term without pressing enter)

"set hidden  " Allow to move between opened files wihtout having to save changes

" Remember last cursor position
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

syntax on

" Highlight trailing spaces in red
highlight TrailingWhitespaces ctermbg=red guibg=red
call matchadd("TrailingWhitespaces", '\s\+$')
